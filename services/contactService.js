// 1.connect to db 
var Contact = require('../models/contact');

// CREATE Contact
exports.createContact = function (contactData, callback) {
    console.log(contactData);
    console.log(callback);
    // 2. constructing the qurey
    var contactDao = new Contact(contactData);
    console.log('id',contactData.customerID);
    Contact.findOne({customerID:contactData.customerID},(err,doc) => {
        if(err) {
            console.log(err);
        }else  if(doc === null) {
            console.log('doc',doc);
           
      contactDao.save(function (err, data) {  //4. get the result
                    if (!err) {
                        console.log(data);
                        var status= {
                            code: 200,
                            msg:'customer added',
                            doc:data
    
                        }
                    } else {
                        console.log(err);
                    }
                   
                    callback(err, status); // 5. send the result
            
                });
            } else{
                var status = {
                    code:201,
                    msg:'alreaady added in the DB'
                }
                callback(err,status)
            }
        })
}
    // 3. executing the query

// GET Contacts
exports.getContacts = function (callback) {
    Contact.find(function(err,data){
        if(!err){
            console.log(data);
        }else{
            console.log(err);
        }
        callback(err,data);
    });
}


// GET Contact by ID
exports.getContactByID = function (requestID, callback) {
    console.log(requestID);
    Contact.findOne({customerID : requestID},function(err,data){
        if(!err){
            console.log(data);
        }else{
            console.log(err);
        }
        callback(err, data);
    });
}

// PUT Contact

exports.updateContact = function (urlID, requestData, callback) {
    console.log(requestData);
    console.log(urlID);
    var query = {customerID: urlID};
    
    console.log(query);
    Contact.findOneAndUpdate(query, requestData, function(err){
        let status; 
        if(!err){
            status = {
                    code: 200,
                    info: 'Update Successfully'
                }
            console.log('Sucess');
        }else{
            console.log(err);
        }
        callback(err,status);
    });
}

