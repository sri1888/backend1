const mongoose = require('./mongo'); //importing connection config
const Schema = mongoose.Schema;

//schema 
const Contact = new Schema(
    {
        customerID: Number,
        name: String,
        phone: String,
        email: String,
        gender: String,
        createdOn: { type: Date, default: Date.now },
        updatedOn: { type: Date, default: Date.now }
    }
);

module.exports = mongoose.model('Contact', Contact);